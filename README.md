-author: Eric Zhou

-contact address email: yixiongz@uoregon.edu

-description: A web based local ACP Brevet Control Times Calculator calculator.
It receive the input data from frontend, send the data to backend by AJAX, after solve the data
the result by be sent back by the same way. 

-reference:https://rusa.org/pages/acp-brevet-control-times-calculator
		   https://rusa.org/octime_acp.html
