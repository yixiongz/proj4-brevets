"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
  """
  Args:
     control_dist_km:  number, the control distance in kilometers
     brevet_dist_km: number, the nominal distance of the brevet
         in kilometers, which must be one of 200, 300, 400, 600,
         or 1000 (the only official ACP brevet distances)
     brevet_start_time:  An ISO 8601 format date-time string indicating
         the official start time of the brevet
  Returns:
     An ISO 8601 format date string indicating the control open time.
     This will be in the same time zone as the brevet start time.
  """
 
  if(control_dist_km > (1.2 * brevet_dist_km)):
    print('Error: control_dist_km is over 20 percent larger than brevet_dist_km')
    return None
    #control point is over 20% longer than the theoretical distance
  
  assert(open_time(1000, 200, brevet_start_time)) == None

  if(control_dist_km < 0 or brevet_dist_km < 0):
    print('Error: distances can not be a nagetive integer')
    return None
    #control_dist_km is a nagative number

  assert(open_time(-1, 200, brevet_start_time)) == None

  if(control_dist_km < brevet_dist_km):
    temp = control_dist_km
  else:
    temp = brevet_dist_km

  if(control_dist_km == 0):
    return brevet_start_time.shift(hours =+ 0, minutes =+ 0).isoformat()

  assert(open_time(0, 200, brevet_start_time)) == brevet_start_time.shift(hours =+ 0, minutes =+ 0).isoformat()

  if(control_dist_km <= 200):
    #control_dist_km in [0,200]

    raw_max_time_200 = temp / 34
    int_hours_200 = raw_max_time_200 // 1
    raw_minutes_200 = (raw_max_time_200 % 1) * 60
    int_minutes_200 = raw_minutes_200 // 1
    return brevet_start_time.shift(hours =+ int_hours_200, minutes =+ int_minutes_200).isoformat()


  elif(control_dist_km <= 400):
    #control_dist_km in [200,400]

    raw_max_time_400 = ((temp - 200) / 32) + (200 / 34)
    int_hours_400 = raw_max_time_400 // 1
    raw_minutes_400 = (raw_max_time_400 % 1) * 60
    int_minutes_400 = raw_minutes_400 // 1
    return brevet_start_time.shift(hours =+ int_hours_400, minutes =+ int_minutes_400).isoformat()

  elif(control_dist_km <= 600):
    #control_dist_km in [400,600]

    raw_max_time_600 = ((temp - 400) / 30) + (200 / 32) + (200 / 34)
    int_hours_600 = raw_max_time_600 // 1
    raw_minutes_600 = (raw_max_time_600 % 1) * 60
    int_minutes_600 = raw_minutes_600 // 1
    return brevet_start_time.shift(hours =+ int_hours_600, minutes =+ int_minutes_600).isoformat()

  elif(control_dist_km <= 1000):
    #control_dist_km in [600,1100]

    raw_max_time_800 = ((temp - 600) / 28) + (200 / 30) + (200 / 32) + (200 / 34) 
    int_hours_800 = raw_max_time_800 // 1
    raw_minutes_800 = (raw_max_time_800 % 1) * 60
    int_minutes_800 = raw_minutes_800 // 1
    return brevet_start_time.shift(hours =+ int_hours_800, minutes =+ int_minutes_800).isoformat()

  else:
    #control_dist_km in [1000,1300]

    raw_max_time_1300 = ((temp - 1000) / 26) + (400 / 28) + (200 / 30) + (200 / 32) + (200 / 34) 
    int_hours_1300 = raw_max_time_1300 // 1
    raw_minutes_1300 = (raw_max_time_1300 % 1) * 60
    int_minutes_1300 = raw_minutes_1300 // 1
    return brevet_start_time.shift(hours =+ int_hours_1300, minutes =+ int_minutes_1300).isoformat()

  assert(open_time(34, 200, brevet_start_time)) ==  brevet_start_time.shift(hours =+ 1, minutes =+ 0).isoformat()
  assert(open_time(340, 400, brevet_start_time)) !=  brevet_start_time.shift(hours =+ 10, minutes =+ 0).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
  """
  Args:
     control_dist_km:  number, the control distance in kilometers
        brevet_dist_km: number, the nominal distance of the brevet
        in kilometers, which must be one of 200, 300, 400, 600, or 1000
        (the only official ACP brevet distances)
     brevet_start_time:  An ISO 8601 format date-time string indicating
         the official start time of the brevet
  Returns:
     An ISO 8601 format date string indicating the control close time.
     This will be in the same time zone as the brevet start time.
  """

  if((1.0 * control_dist_km) > (1.2 * brevet_dist_km)):
    print('Error: control_dist_km is over 20 percent larger than brevet_dist_km')
    return None
    #control point is over 20% longer than the theoretical distance

  assert(open_time(1000, 200, brevet_start_time)) == None

  if(control_dist_km < 0 or brevet_dist_km < 0):
    print('Error: distances can not be a nagetive integer')
    return None
    #control_dist_km is a nagative number

  assert(open_time(-1, 200, brevet_start_time)) == None

  if(control_dist_km < brevet_dist_km):
    temp = control_dist_km
  else:
    temp = brevet_dist_km

  if(control_dist_km == 0):
    return brevet_start_time.shift(hours =+ 1, minutes =+ 0).isoformat()

  assert(open_time(0, 200, brevet_start_time)) == brevet_start_time.shift(hours =+ 0, minutes =+ 0).isoformat()

  if (control_dist_km <= 200):
    #control_dist_km in [0,200]

    raw_min_time_200 = temp / 15
    int_hours_200 = raw_min_time_200 // 1
    raw_minutes_200 = (raw_min_time_200 % 1) * 60
    int_minutes_200 = raw_minutes_200 // 1
    return brevet_start_time.shift(hours =+ int_hours_200, minutes =+ int_minutes_200).isoformat()

  elif(control_dist_km <= 400):
    #control_dist_km in [200,400]

    raw_min_time_400 = ((temp - 200) / 15) + (200 / 15)
    int_hours_400 = raw_min_time_400 // 1
    raw_minutes_400 = (raw_min_time_400 % 1) * 60
    int_minutes_400 = raw_minutes_400 // 1
    return brevet_start_time.shift(hours =+ int_hours_400, minutes =+ int_minutes_400).isoformat()

  elif(control_dist_km <= 600):
    #control_dist_km in [400,600]

    raw_min_time_600 = ((temp - 400) / 15) + (200 / 15) + (200 / 15)
    int_hours_600 = raw_min_time_600 // 1
    raw_minutes_600 = (raw_min_time_600 % 1) * 60
    int_minutes_600 = raw_minutes_600 // 1
    return brevet_start_time.shift(hours =+ int_hours_600, minutes =+ int_minutes_600).isoformat()

  elif(control_dist_km <= 1000):
    #control_dist_km in [600,1100]

    raw_min_time_800 = ((temp - 600) / 11.428) + (200 / 15) + (200 / 15) + (200 / 15) 
    int_hours_800 = raw_min_time_800 // 1
    raw_minutes_800 = (raw_min_time_800 % 1) * 60
    int_minutes_800 = raw_minutes_800 // 1
    return brevet_start_time.shift(hours =+ int_hours_800, minutes =+ int_minutes_800).isoformat()

  else:
    #control_dist_km in [1000,1300]

    raw_min_time_1300 = ((temp - 1000) / 13.333) + (400 / 11.428) + (200 / 15) + (200 / 15) + (200 / 15) 
    int_hours_1300 = raw_min_time_1300 // 1
    raw_minutes_1300 = (raw_min_time_1300 % 1) * 60
    int_minutes_1300 = raw_minutes_1300 // 1
    return brevet_start_time.shift(hours =+ int_hours_1300, minutes =+ int_minutes_1300).isoformat()

  assert(open_time(15, 200, brevet_start_time)) ==  brevet_start_time.shift(hours =+ 1, minutes =+ 0).isoformat()
  assert(open_time(150, 400, brevet_start_time)) !=  brevet_start_time.shift(hours =+ 10, minutes =+ 0).isoformat()



#Reference:
# https://blog.csdn.net/DAGU131/article/details/79365301
# http://support.sas.com/documentation/cdl/en/lrdict/64316/HTML/default/viewer.htm#a003169814.htm
